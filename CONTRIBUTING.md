## Como desarrollador

* Clona este repositorio en tu cuenta
* Crea una rama con alguna funcionalidad, mejora o problema. Importante, siempre a partir de la rama develop
* En cuanto la tengas lista, empuja tu rama a tu repositorio
* Crea un Pull Request, siempre hacía nuestra rama **develop**

## Como usuario

* Usa Empresa Libre
* Prueba que todo funcione
* Crea nueva documentación
* Propon nuevas funcionalidades
* Difunde Empresa Libre


### Prioridades

1. Darle continuidad a la facturación de los clientes
2. Cubrir todas las funcionalidades que cubre ahora Factura Libre
3. Agregar nuevas funcionalidades
