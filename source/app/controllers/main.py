#!/usr/bin/env python3

import falcon
from middleware import get_template


class AppEmpresas(object):
    template = 'empresas.html'

    def __init__(self, db):
        self._db = db

    @falcon.after(get_template)
    def on_get(self, req, resp):
        values = req.params
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.empresa_agregar(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.empresa_borrar(values):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204



class AppLogin(object):
    template = 'login.html'

    def __init__(self, db):
        self._db = db

    @falcon.after(get_template)
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        session = req.env['beaker.session']
        values = req.params
        values['rfc'] = values['rfc'].upper()
        result, user = self._db.authenticate(values)
        if result['login']:
            session.save()
            session['userobj'] = user
            session['user'] = result['user']
            session['rfc'] = values['rfc']
        req.context['result'] = result
        resp.status = falcon.HTTP_200


class AppLogout(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        session = req.env['beaker.session']
        session.delete()
        resp.status = falcon.HTTP_200
        raise falcon.HTTPTemporaryRedirect('/')


class AppAdmin(object):
    template = 'admin.html'

    def __init__(self, db):
        self._db = db

    @falcon.after(get_template)
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200


class AppMain(object):
    template = 'main.html'

    def __init__(self, db):
        self._db = db

    @falcon.after(get_template)
    def on_get(self, req, resp):
        resp.status = falcon.HTTP_200


class AppValues(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp, table):
        values = req.params
        session = req.env['beaker.session']
        if table == 'admin':
            req.context['result'] = session['userobj'].es_superusuario \
                or session['userobj'].es_admin
        else:
            req.context['result'] = self._db.get_values(table, values, session)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp, table):
        values = req.params
        session = req.env['beaker.session']
        if table == 'usuario' and (session['userobj'].id == int(values['id'])):
            resp.status = falcon.HTTP_204
            return

        if self._db.delete(table, values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204

    def on_post(self, req, resp, table):
        file_object = req.get_param('upload')
        if file_object is None:
            session = req.env['beaker.session']
            values = req.params
            if table == 'correo':
                req.context['result'] = self._db.validate_email(values)
            elif table == 'sendmail':
                req.context['result'] = self._db.send_email(values, session)
            elif table == 'enviarprefac':
                req.context['result'] = self._db.enviar_prefac(values)
            elif table == 'addunidad':
                req.context['result'] = self._db.add_unidad(values)
            elif table == 'addimpuesto':
                req.context['result'] = self._db.add_impuesto(values)
            elif table == 'addusuario':
                req.context['result'] = self._db.add_usuario(values)
            elif table == 'editusuario':
                req.context['result'] = self._db.edit_usuario(values)
            elif table == 'bdfl':
                req.context['result'] = self._db.importar_bdfl()
            else:
                req.context['result'] = self._db.validate_cert(values, session)
        else:
            req.context['result'] = self._db.add_cert(file_object)
        resp.status = falcon.HTTP_200


class AppFiles(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp, table):
        values = req.params
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp, table):
        session = req.env['beaker.session']
        file_object = req.get_param('upload')
        req.context['result'] = self._db.upload_file(session, table, file_object)
        resp.status = falcon.HTTP_200


class AppConfig(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_config(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.add_config(values)
        resp.status = falcon.HTTP_200



class AppPartners(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_partners(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.partner(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('partner', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppProducts(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_products(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.product(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('product', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppInvoices(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_invoices(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.invoice(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('invoice', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppPreInvoices(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_preinvoices(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.preinvoice(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('preinvoice', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppEmisor(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        session = req.env['beaker.session']
        req.context['result'] = self._db.get_emisor(session['rfc'])
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.emisor(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('invoice', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppCuentasBanco(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        session = req.env['beaker.session']
        req.context['result'] = self._db.get_cuentasbanco(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.cuentasbanco(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('cuentasbanco', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppMovimientosBanco(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        session = req.env['beaker.session']
        req.context['result'] = self._db.get_movimientosbanco(values)
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.add_movbanco(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('movbanco', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppFolios(object):

    def __init__(self, db):
        self._db = db

    def on_get(self, req, resp):
        values = req.params
        req.context['result'] = self._db.get_folios()
        resp.status = falcon.HTTP_200

    def on_post(self, req, resp):
        values = req.params
        req.context['result'] = self._db.add_folios(values)
        resp.status = falcon.HTTP_200

    def on_delete(self, req, resp):
        values = req.params
        if self._db.delete('folios', values['id']):
            resp.status = falcon.HTTP_200
        else:
            resp.status = falcon.HTTP_204


class AppDocumentos(object):

    def __init__(self, db):
        self._db = db
        #~ self._not_json = True

    def on_get(self, req, resp, type_doc, id_doc):
        session = req.env['beaker.session']
        req.context['result'], file_name, content_type = \
            self._db.get_doc(type_doc, id_doc, session['rfc'])
        resp.append_header('Content-Disposition',
            'attachment; filename={}'.format(file_name))
        resp.content_type = content_type
        resp.status = falcon.HTTP_200
