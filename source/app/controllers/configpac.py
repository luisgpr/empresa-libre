#!/usr/bin/env python


from .conf import DEBUG, ID_INTEGRADOR, FINKOK

DEBUG = DEBUG
TIMEOUT = 10

#~ PACs que han proporcionado un entorno de pruebas libre y abierto
#~ ecodex, finkok
PAC = 'finkok'


def ecodex(debug):
    NEW_SERVER = True
    auth = {'ID': ID_INTEGRADOR}
    if debug:
        #~ No cambies este ID de pruebas
        auth = {'ID': '2b3a8764-d586-4543-9b7e-82834443f219'}

    base_url = 'https://servicios.ecodex.com.mx:4043/Servicio{}.svc?wsdl'
    if NEW_SERVER:
        base_url = 'https://serviciosnominas.ecodex.com.mx:4043/Servicio{}.svc?wsdl'
        base_api = 'https://api.ecodex.com.mx/{}'
    if debug:
        base_url = 'https://wsdev.ecodex.com.mx:2045/Servicio{}.svc?wsdl'
        base_api = 'https://pruebasapi.ecodex.com.mx/{}'
    url = {
        'seguridad': base_url.format('Seguridad'),
        'clients': base_url.format('Clientes'),
        'timbra': base_url.format('Timbrado'),
        'token': base_api.format('token?version=2'),
        'docs': base_api.format('api/documentos'),
        'hash': base_api.format('api/Documentos/{}'),
        'codes': {
            'HASH': 'DUPLICIDAD EN HASH',
        }
    }
    return auth, url


#~ IMPORTANTE: Si quieres hacer pruebas, con tu propio correo de usuario y
#~ contraseña, ponte en contacto con Finkok para que te asignen tus datos de
#~ acceso, consulta su documentación para ver las diferentes opciones de acceso.
#~ Si solo estas haciendo pruebas de timbrado y ancelación, con estos datos debería
#~ ser suficiente.
def finkok(debug):
    USER = FINKOK['USER']
    PASS = FINKOK['PASS']
    TOKEN = ''
    auth = {
        'USER': '',
        'PASS': TOKEN or PASS,
        'RESELLER': {'USER': USER, 'PASS': PASS}
    }
    if debug:
        USER = 'pruebas-finkok@correolibre.net'
        PASS = ''
        TOKEN = '5c9a88da105bff9a8c430cb713f6d35269f51674bdc5963c1501b7316366'
        auth = {
            'USER': USER,
            'PASS': TOKEN or PASS,
            'RESELLER': {
                'USER': '',
                'PASS': ''
            }
        }

    base_url = 'https://facturacion.finkok.com/servicios/soap/{}.wsdl'
    if debug:
        base_url = 'http://demo-facturacion.finkok.com/servicios/soap/{}.wsdl'
    url = {
        'timbra': base_url.format('stamp'),
        'quick_stamp': False,
        'cancel': base_url.format('cancel'),
        'client': base_url.format('registration'),
        'util': base_url.format('utilities'),
        'codes': {
            '200': 'Comprobante timbrado satisfactoriamente',
            '307': 'Comprobante timbrado previamente',
            '205': 'No Encontrado',
        }
    }
    return auth, url


AUTH, URL = globals()[PAC](DEBUG)

