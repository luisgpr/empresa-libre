

var menu_data = [
    {id: 'app_home', icon: 'dashboard', value: 'Inicio'},
    {id: 'app_partners', icon: 'users', value: 'Clientes y Proveedores'},
    {id: 'app_products', icon: 'server', value: 'Productos y Servicios'},
    {id: 'app_bancos', icon: 'university', value: 'Bancos'},
    {id: 'app_invoices', icon: 'cart-plus', value: 'Facturas'},
]


var sidebar = {
    view: 'sidebar',
    data: menu_data,
    ready: function(){
        this.select('app_home');
        this.open(this.getParentId('app_home'));
    },
    on:{
        onAfterSelect: function(id){
            $$('multi').setValue(id)
        }
    },
}


var multi_main = {
    id: 'multi',
    animate: true,
    cells:[
        {
            id: 'app_home',
            view: 'template',
            template: 'HOME'
        },
        app_partners,
        app_products,
        app_bancos,
        app_invoices,
    ],
}


var menu_user = {
    view: 'menu',
    id: 'menu_user',
    width: 150,
    autowidth: true,
    data: [
        {id: '0', value: 'User...', submenu:[{id:1, value:'Cerrar Sesión'}]},
    ],
    type: {
        subsign: true,
    },
}


var ui_main = {
    rows: [
        {view: 'toolbar', padding: 3, elements: [
            {view: 'button', type: 'icon', icon: 'bars',
                width: 37, align: 'left', css: 'app_button', click: function(){
                    $$('$sidebar1').toggle()
                }
            },
            {view: 'label', id: 'lbl_title_main', label: '<b>Empresa Libre</b>'},
            {},
            menu_user,
            {view: 'button', type: 'icon', width: 45, css: 'app_button',
                icon: 'bell-o',  badge: 0},
            {view: 'button', type: 'icon', width: 45, css: 'app_button',
                icon: 'cogs',  id: 'cmd_ir_al_admin', hidden: true,
                click: 'cmd_ir_al_admin_click'}
        ]
        },
        {
            cols:[
                sidebar,
                multi_main,
            ]
        }
    ]
};
