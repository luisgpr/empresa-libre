
var msg_rfc = 'El RFC es requerido'

var form_controls_empresa = [
    {view: 'text', label: 'RFC', id: 'txt_alta_rfc', name: 'alta_rfc',
        labelPosition: 'top', required: true, invalidMessage: msg_rfc},
    {margin: 10, cols:[{}, {view: 'button', value: 'Agregar RFC',
        click: 'validate_nuevo_rfc', hotkey: 'enter'}, {}]}
]


var msg_header = '<font color="#610B0B">Bienvenido a Empresa Libre</font>'
var header = [
    {view: 'label', label: '<b><font color="#610B0B">Alta de Emisor</font></b>'},
    {},
    {view: 'button', type: 'icon', width: 40, css: 'app_button',
        icon: 'home', click: 'window.location = "/"'},
]


var grid_empresas_cols = [
    {id: 'delete', header: '', width: 30, css: 'delete'},
    {id: 'rfc', header: 'RFC Emisor', fillspace: true,
        footer: {content: 'rowCount', css: 'right'}},
]


var grid_empresas = {
    view: 'datatable',
    id: 'grid_empresas',
    select: 'row',
    url: '/values/empresas',
    adjust: true,
    autoheight: true,
    headermenu: true,
    footer: true,
    columns: grid_empresas_cols,
}


var ui_empresas = {
    rows: [
        {maxHeight: 50},
        {view: 'template', template: msg_header, maxHeight: 50, css: 'login_header'},
        {maxHeight: 50},
        {cols: [{}, {type: 'space', padding: 5,
            rows: [
                {view: 'toolbar', elements: header},
                {
                    container: 'form_empresas',
                    view: 'form',
                    id: 'form_empresas',
                    width: 400,
                    elements: form_controls_empresa,
                    rules:{
                        alta_rfc:function(value){ return value.trim() != '';},
                    }
                },
                grid_empresas,
            ]}, {}, ]
        },
    ]
}
