

var toolbar_partners = [
    {view: 'button', id: 'cmd_new_partner', label: 'Nuevo', type: 'iconButton',
        autowidth: true, icon: 'user-plus'},
    {view: 'button', id: 'cmd_edit_partner', label: 'Editar', type: 'iconButton',
        autowidth: true, icon: 'user'},
    {view: 'button', id: 'cmd_delete_partner', label: 'Eliminar', type: 'iconButton',
        autowidth: true, icon: 'user-times'},
]


var grid_partners_cols = [
    {id: 'index', header:'#', css: 'right',
        footer: {content: 'rowCount', colspan: 2, css: 'right'}},
    {id: 'id', header: 'Clave', sort: 'int', css: 'right'},
    {id: 'rfc', header: ['RFC', {content: 'textFilter'}], adjust: 'data',
        sort: 'string', footer: {text: 'Clientes y Proveedores', colspan: 2}},
    {id: 'nombre', header: ['Razón Social', {content: 'textFilter'}],
        fillspace:true, sort: 'string'},
    {id: 'saldo_cliente', header: ['Saldo Cliente', {content: 'numberFilter'}],
        width: 150, sort: 'int', format: webix.i18n.priceFormat, css: 'right'},
]


var grid_partners = {
    view: 'datatable',
    id: 'grid_partners',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_partners_cols,
    pager: 'pager_clientes',
    //~ datafetch: 100,
    //~ loadahead: 100,
    //~ url: '/partners',
    ready:function(){
        this.adjustColumn('index');
        this.adjustColumn('id');
        this.adjustColumn('rfc');
    },
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i+1;
            })
        }
    },
}


var persons_type = [
    {id: 1, value: 'Física'},
    {id: 2, value: 'Moral'},
    {id: 3, value: 'Público'},
    {id: 4, value: 'Extranjero'},
]


var controls_fiscales = [
    {template: 'Tipo de Persona', type: 'section'},
    {view: 'radio', id: 'tipo_persona', name: 'tipo_persona', label: 'Tipos: ',
        labelWidth: 150, value: 1, options: persons_type, required: true,
        invalidMessage: 'El Tipo de Persona es requerido'},
    {template: 'Dirección Fiscal', type: 'section'},
    {cols: [{view: 'text', id: 'rfc', name: 'rfc', label: 'RFC: ', width: 300,
        required: true, invalidMessage: 'RFC inválido', attributes: {maxlength: 13}},{}]},
    {view: 'text', id: 'nombre', name: 'nombre', label: 'Razón Social: ', required: true,
        invalidMessage: 'La Razón Social es requerida'},
    {view: 'text', id: 'calle', name: 'calle', label: 'Calle: '},
    {cols: [{view: 'text', id: 'no_exterior', name: 'no_exterior', width: 300,
        label: 'No Exterior: '},{}]},
    {cols: [{view: 'text', id: 'no_interior', name: 'no_interior', width: 300,
        label: 'No Interior: '},{}]},
    {cols: [{view: 'search', id: 'codigo_postal', name: 'codigo_postal',
        width: 300, label: 'C.P.: ', attributes: {maxlength: 5}},{}]},
    {view: 'text', id: 'colonia', name: 'colonia', label: 'Colonia: '},
    {view: 'text', id: 'municipio', name: 'municipio', label: 'Municipio: '},
    {view: 'text', id: 'estado', name: 'estado', label: 'Estado: '},
    {view: 'text', id: 'pais', name: 'pais', label: 'País: ',
        value: 'México', readonly: true},
    {template: 'Condiciones Comerciales', type: 'section'},
    {cols: [
        {view: 'richselect', id: 'forma_pago', name: 'forma_pago',
            label: 'Forma de Pago: ', required: true, options: [],
            invalidMessage: 'La Forma de pago es requerida'},
        {view: 'text', id: 'condicion_pago', name: 'condicion_pago',
            label: 'Condiciones de Pago: ', suggest: []},
    ]},
    {cols: [
        {view: 'counter', id: 'dias_pago', name: 'dias_pago',
            label: 'Días de pago', step: 5, value: 0, min: 0, max: 365,
            tooltip: 'Permite calcular las fechas de pago', width: 250},
        {view: 'checkbox', id: 'dias_habiles', name: 'dias_habiles',
            label: 'Hábiles: ', value: false, width: 180},
        {},
    ]},
    {cols: [
        {view: 'richselect', id: 'lst_uso_cfdi_socio', name: 'uso_cfdi_socio',
            label: 'Uso del CFDI', options: []},
        {},
    ]}
]


var controls_others = [
    {view: 'checkbox', id: 'es_activo', name: 'es_activo', label: 'Activo: ',
        value: true, bottomLabel: '&nbsp;&nbsp;&nbsp;Se recomienda solo desactivar y no eliminar'},
    {view: 'text', id: 'commercial_name', name: 'nombre_comercial',
        label: 'Nombre Comercial: '},
    {view: 'text', id: 'telefonos', name: 'telefonos', label: 'Teléfonos: '},
    {view: 'text', id: 'web', name: 'web', label: 'Página Web: '},
    {view: 'text', id: 'correo_facturas', name: 'correo_facturas',
        label: 'Correos para Facturas: ', tooltip: 'Separados por comas',
        bottomLabel: 'Uno o más correos electrónicos separados por comas'},
    {cols: [
        {view: 'checkbox', id: 'es_cliente', name: 'es_cliente',
            label: 'Es Cliente: ', value: true, width: 180},
        {view: 'text', id: 'cuenta_cliente', name: 'cuenta_cliente',
            label: 'Cuenta Cliente: ', disabled: true}, {}]
    },
    {cols: [
        {view: 'checkbox', id: 'es_proveedor', name: 'es_proveedor',
            label: 'Es Proveedor: ', value: false, width: 180},
        {view: 'text', id: 'cuenta_proveedor', name: 'cuenta_proveedor',
            label: 'Cuenta Proveedor: ', disabled: true}, {}]
    },
    {view: 'checkbox', name: 'es_ong', label: 'Es ONG: ', value: false},
    {view: 'text', name: 'tags', label: 'Etiquetas',
        tooltip: 'Utiles para filtrados rápidos. Separa por comas.'},
    {view: 'textarea' , height: 200, name: 'notas', label: 'Notas'},
]



var toolbar_contacts = [
    {view: 'button', id: 'cmd_new_contact', label: 'Nuevo', type: 'iconButton',
        autowidth: true, icon: 'user-plus'},
]


var grid_contacts_cols = [
    {id: 'index', header: '#', adjust:'data', css:'right',
        footer: {content: 'rowCount'}},
    {id: 'id', header: '', hidden: true},
    {id: 'title', header: 'Título', adjust:'data', sort: 'string',
        footer: 'Contactos'},
    {id: 'first_name', header: ['Nombre', {content: 'textFilter'}], adjust:'data',
        sort: 'string'},
    {id: 'paterno', header: ['Apellido Paterno', {content: 'textFilter'}],
        fillspace:true, sort: 'string'},
    {id: 'materno', header: ['Apellido Materno', {content: 'textFilter'}],
        fillspace:true, sort: 'string'},
    {id: 'date_born', header: ['Fecha de Nacimiento'],
        adjust: 'data'},
]


var grid_contacts = {
    view: 'datatable',
    id: 'grid_contacts',
    select: 'row',
    adjust: true,
    height: 400,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_contacts_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_email = {
    view: 'datatable',
    id: 'grid_email',
    select: 'row',
    adjust: true,
    height: 200,
    resizeColumn: true,
    columns: [
        {id: 'id', header: '', hidden: true},
        {id: 'type', header: 'Tipo', adjust:'data', sort: 'string'},
        {id: 'email', header: ['Correo Electrónico'], fillspace:true, sort: 'string'},
    ],
}


var grid_phone = {
    view: 'datatable',
    id: 'grid_phone',
    select: 'row',
    adjust: true,
    height: 200,
    resizeColumn: true,
    columns: [
        {id: 'id', header: '', hidden: true},
        {id: 'type', header: 'Tipo', adjust:'data', sort: 'string'},
        {id: 'phone', header: ['Teléfono'], fillspace:true, sort: 'string'},
    ],
}


var controls_contact = [
    {template: 'Datos Generales', type: 'section' },
    {cols: [
        {view: 'combo', id: 'cbo_title_contact', name: 'cbo_title_contact',
            label: 'Título: ', width: 150},
        {view: 'text', id: 'name_contact', name: 'name_contact',
            label: 'Nombre: '},
        {view: 'text', id: 'paterno_contact', name: 'paterno_contact',
            label: 'A. Paterno: '},
        {view: 'text', id: 'materno_contact', name: 'materno_contact',
            label: 'A. Materno: '},
        {view: 'datepicker', id: 'date_contact', name: 'date_contact',
            label: 'Fecha de Nacimiento: ', format: '%d-%M-%Y'},
        ]},
    {template: 'Correos y Teléfonos', type: 'section' },
    {cols: [
        grid_email,
        {maxWidth: 10},
        grid_phone,
        ]},
    {},
    { template:"", type: "section" },
    { margin: 10, cols: [{},
        {view: "button", id: "cmd_save_conctact", label: "Guardar Contacto" , type: "form", autowidth: true, align:"center"},
        {view: "button", id: "cmd_cancel_contact", label: "Regresar" , type: "danger", autowidth: true, align:"center"},
        {}]
    },

]


var form_contact = {
    type: 'space',
    cols: [{
        view: 'form',
        id: 'form_contact',
        complexData: true,
        elements: controls_contact,
        elementsConfig: {
            labelPosition: 'top',
        },
        autoheight: true,
    }]
}


var multi_contacts = {
    id: 'multi_contacts',
    animate: true,
    cells:[
        {id: 'contacts_home', rows:[
            {view: 'toolbar', elements: toolbar_contacts},
            grid_contacts,
        ]},
        {id: 'contacts_new', rows:[form_contact, {}]}
    ]
};


var controls_partner = [
    {
        view: 'tabview',
        id: 'tab_partner',
        tabbar: {options: ['Datos Fiscales', 'Otros Datos', 'Contactos']}, animate: true,
        cells: [
            {id: 'Datos Fiscales', rows: controls_fiscales},
            {id: 'Otros Datos', rows: controls_others},
            {id: 'Contactos', rows: [multi_contacts]},
        ]
    },
    {rows: [
        { template:"", type: "section" },
        { margin: 10, cols: [{},
            {view: "button", id: "cmd_save_partner", label: "Guardar" , type: "form", autowidth: true, align:"center"},
            {view: "button", id: "cmd_cancel_partner", label: "Cancelar" , type: "danger", autowidth: true, align:"center"},
            {}]
        },
    ]}
]


var form_partner = {
    type: 'space',
    cols: [{
        view: 'form',
        id: 'form_partner',
        complexData: true,
        scroll: true,
        elements: controls_partner,
        elementsConfig: {
            labelWidth: 150,
            labelAlign: 'right'
        },
        rules: {
            nombre: function(value){ return value.trim() != '';},
            rfc: validate_rfc,
        }
    }]
}


var pager_clientes = {
    view: "pager",
    id: "pager_clientes",
    template: "{common.prev()} {common.pages()} {common.next()}",
    size: 100,
    group: 10,
}


var multi_partners = {
    id: 'multi_partners',
    animate: true,
    cells:[
        {id: 'partners_home', rows:[
            {view: 'toolbar', elements: toolbar_partners},
            grid_partners,
            pager_clientes,
        ]},
        {id: 'partners_new', rows:[form_partner]}
    ]
}


var title_partners = 'Administración de Clientes y Proveedores'
var app_partners = {
    id: 'app_partners',
    rows:[
        {view: 'template', id: 'th_partner', type: 'header', template: title_partners},
        multi_partners
    ]
}


