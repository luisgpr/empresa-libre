

var grid_cfdi_cliente_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'rowCount', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie", {content: "selectFilter"}], adjust: "header",
        sort:"string"},
    {id: 'folio', header: ['Folio'], adjust: 'data', sort: 'int',
        css: 'right'},
    {id: 'uuid', header: ['UUID', {content: 'textFilter'}], width: 250,
        sort: 'string'},
    {id: "fecha", header: ["Fecha y Hora"], width: 150, sort: 'string'},
    {id: "tipo_comprobante", header: ["Tipo", {content: "selectFilter"}],
        adjust: 'header', sort: 'string'},
    {id: "estatus", header: ["Estatus", {content: "selectFilter"}],
        adjust: "header", sort:"string"},
    {id: 'total_mn', header: ['Total M.N.'], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right'},
]


var grid_relacionados_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right'},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: "Serie", adjust: "header", sort:"string"},
    {id: 'folio', header: 'Folio', adjust: 'data', sort: 'int', css: 'right'},
    {id: 'uuid', header: 'UUID', width: 250, sort: 'string'},
    {id: "fecha", header: "Fecha y Hora", width: 150, sort: 'date'},
    {id: "tipo_comprobante", header: "Tipo", adjust: 'header', sort: 'string'},
    {id: "estatus", header: "Estatus", adjust: "header", sort:"string"},
    {id: 'total_mn', header: ['Total M.N.'], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right'},
]


var grid_cfdi_cliente = {
    view: 'datatable',
    id: 'grid_cfdi_cliente',
    select: 'row',
    autoConfig: false,
    adjust: true,
    height: 300,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    columns: grid_cfdi_cliente_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    }
}


var grid_relacionados = {
    view: 'datatable',
    id: 'grid_relacionados',
    select: 'row',
    autoConfig: false,
    adjust: true,
    height: 200,
    resizeColumn: true,
    headermenu: true,
    drag: true,
    columns: grid_relacionados_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    }
}


var body_cfdi_relacionados = {rows: [
    {cols: [
        {view: 'label', id: 'lbl_cfdi_title', label: 'Cliente: ',
            autowidth: true},
        {view: 'label', id: 'lbl_cfdi_cliente', label: '', align: 'left'}]},
    {cols: [
        {view: 'richselect', id: 'lst_tipo_relacion', label: 'Tipo de Relación',
            labelWidth: 140, required: true, options: []},
        {view: 'checkbox', id: 'chk_relacionados_anticipo',
            labelRight: 'Solo Anticipos', width: 200, disabled: true}]},
    {minHeight: 10, maxHeight: 10},
    {cols: [
        {view: 'richselect', id: 'filter_cfdi_year', label: 'Año', width: 100,
            labelAlign: 'center', labelPosition: 'top', options: []},
        {view: 'richselect', id: 'filter_cfdi_month', label: 'Mes', width: 125,
            labelAlign: 'center', labelPosition: 'top', options: months},
        {view: 'text', id: 'filter_cfdi_folio', label: 'Folio', width: 125,
            labelAlign: 'center', labelPosition: 'top'},
        {view: 'text', id: 'filter_cfdi_uuid', label: 'UUID',
            labelAlign: 'center', labelPosition: 'top'},
        {view: 'icon', id: 'cmd_filter_relacionados', icon: 'filter'},
    ]},
    grid_cfdi_cliente,
    {minHeight: 10, maxHeight: 10},
    {view: 'label', label: 'CFDI Relacionados'},
    grid_relacionados,
    {minHeight: 10, maxHeight: 10},
    {cols: [{},
        {view: 'button', id: 'cmd_guardar_relacionados', label: 'Relacionar'},
        {view: 'button', id: 'cmd_limpiar_relacionados', label: 'Limpiar'},
        {}]},
    {minHeight: 15, maxHeight: 15},
]}


var ui_invoice = {
    init: function(){
        webix.ui({
            view: 'window',
            id: 'win_cfdi_relacionados',
            autoheight: true,
            width: 850,
            modal: true,
            position: 'center',
            head: {view: 'toolbar',
                elements: [
                    {view: 'label', label: 'CFDI Relacionados'},
                    {view: 'icon', icon: 'times-circle',
                        click: '$$("win_cfdi_relacionados").close()'},
                ]
            },
            body: body_cfdi_relacionados,
        })
        $$('cmd_guardar_relacionados').attachEvent('onItemClick', cmd_guardar_relacionados_click)
        $$('cmd_limpiar_relacionados').attachEvent('onItemClick', cmd_limpiar_relacionados_click)
        $$('cmd_filter_relacionados').attachEvent('onItemClick', cmd_filter_relacionados_click)
        $$('lst_tipo_relacion').attachEvent('onChange', lst_tipo_relacion_change)
        $$('filter_cfdi_month').attachEvent('onChange', filter_cfdi_month_change)
        $$('filter_cfdi_month').attachEvent('onChange', filter_cfdi_month_change)
}}


var toolbar_invoices = [
    {view: "button", id: "cmd_new_invoice", label: "Nueva", type: "iconButton",
        autowidth: true, icon: "plus"},
    {view: "button", id: "cmd_refacturar", label: "Refacturar", type: "iconButton",
        autowidth: true, icon: "pencil"},
    {},
    {view: "button", id: "cmd_delete_invoice", label: "Eliminar", type: "iconButton",
        autowidth: true, icon: "minus"},
]


var toolbar_invoices_util = [
    {view: 'button', id: 'cmd_invoice_timbrar', label: 'Timbrar',
        type: 'iconButton', autowidth: true, icon: 'ticket'},
    {view: 'button', id: 'cmd_invoice_sat', label: 'SAT',
        type: 'iconButton', autowidth: true, icon: 'check-circle'},
    {},
    {view: 'button', id: 'cmd_invoice_cancelar', label: 'Cancelar',
        type: 'iconButton', autowidth: true, icon: 'ban'},
]


var toolbar_invoices_generate = {view: 'toolbar', elements: [{},
    {view: 'button', id: 'cmd_cfdi_relacionados', label: 'CFDI Relacionados',
        type: 'iconButton', autowidth: true, icon: 'file-o'},
    {view: 'checkbox', id: 'chk_cfdi_anticipo', labelRight: 'Es Anticipo',
        labelWidth: 0, width: 100, hidden: true},
    {view: 'checkbox', id: 'chk_cfdi_donativo', labelRight: 'Es Donativo',
        labelWidth: 0, width: 100, hidden: true},
{}]}


var toolbar_invoices_filter = [
    {view: 'richselect', id: 'filter_year', label: 'Año', labelAlign: 'right',
        labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'filter_month', label: 'Mes', labelAlign: 'right',
        labelWidth: 50, width: 200, options: months},
    {view: 'daterangepicker', id: 'filter_dates', label: 'Fechas',
        labelAlign: 'right', width: 300},
]


function get_icon(tipo){
    icons = {
        xml: 'fa-file-code-o',
        pdf: 'fa-file-pdf-o',
        zip: 'fa-file-zip-o',
        email: 'fa-envelope-o',
    }
    return "<span class='webix_icon " + icons[tipo] + "'></span>"
}


var grid_invoices_cols = [
    {id: 'index', header: '#', adjust: 'data', css: 'right',
        footer: {content: 'rowCount', colspan: 3, css: 'right'}},
    {id: "id", header:"ID", hidden:true},
    {id: "serie", header: ["Serie", {content: "selectFilter"}], adjust: "data",
        sort:"string"},
    {id: 'folio', header: ['Folio', {content: 'numberFilter'}], adjust: 'data',
        sort: 'int', css: 'right', footer: {text: 'Facturas', colspan: 3}},
    {id: "uuid", header: ["UUID", {content: "textFilter"}], adjust: "data",
        sort:"string", hidden:true},
    {id: "fecha", header: ["Fecha y Hora"],
        adjust: "data", sort: "string"},
    {id: "tipo_comprobante", header: ["Tipo", {content: "selectFilter"}],
        adjust: 'header', sort: 'string'},
    {id: "estatus", header: ["Estatus", {content: "selectFilter"}],
        adjust: "data", sort:"string"},
    {id: 'total_mn', header: ['Total M.N.', {content: 'numberFilter'}], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right'},
    {id: "cliente", header: ["Razón Social", {content: "selectFilter"}],
        fillspace:true, sort:"string"},
    {id: 'xml', header: 'XML', adjust: 'data', template: get_icon('xml')},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
    {id: 'zip', header: 'ZIP', adjust: 'data', template: get_icon('zip')},
    {id: 'email', header: '', adjust: 'data', template: get_icon('email')}
]


var grid_invoices = {
    view: 'datatable',
    id: 'grid_invoices',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    headermenu: true,
    columns: grid_invoices_cols,
    on:{
        'data->onStoreUpdated':function(){
            this.data.each(function(obj, i){
                obj.index = i + 1
            })
        }
    },
}


var grid_details_cols = [
    {id: "id", header:"ID", hidden: true},
    {id: 'delete', header: '', width: 30, css: 'delete'},
    {id: "clave", header:{text: 'Clave', css: 'center'}, width: 100},
    {id: "clave_sat", hidden: true},
    {id: "descripcion", header:{text: 'Descripción', css: 'center'},
        fillspace: true, editor: 'text'},
    {id: "unidad", header:{text: 'Unidad', css: 'center'}, width: 100},
    {id: 'cantidad', header: {text: 'Cantidad', css: 'center'}, width: 100,
        format: webix.i18n.numberFormat, css: 'right', editor: 'text'},
    {id: "valor_unitario", header:{text: 'Valor Unitario', css: 'center'},
        width: 100, format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
    {id: 'descuento', header:{text: 'Descuento', css: 'center'},
        width: 80, format: webix.i18n.priceFormat, css: 'right', editor: 'text'},
    {id: 'precio_final', hidden: true, header: 'precio_final', width: 80,
        format: webix.i18n.priceFormat, css: 'right'},
    {id: "importe", header:{text: 'Importe', css: 'center'}, width: 150,
        format: webix.i18n.priceFormat, css: 'right'},
]


var grid_details = {
    view: 'datatable',
    id: 'grid_details',
    select: 'row',
    adjust: true,
    autoheight: true,
    editable: true,
    columns: grid_details_cols,
    data: [],
}


var grid_totals_cols = [
    {id: 'id', header: 'ID', hidden: true},
    {id: 'concepto', header: 'Concepto', width: 200,
        footer: {text: 'TOTAL', css:'right_footer'}, css:'right'},
    {id: 'importe', header: 'Importe', width: 150,
        footer: {content: 'summColumn', css:'right_footer'},
        format: webix.i18n.priceFormat, css:'right'},
]


var grid_totals = {
    view: 'datatable',
    id: 'grid_totals',
    select: false,
    width: 350,
    header: false,
    footer: true,
    autoheight: true,
    columns: grid_totals_cols,
    data: [{id: 1, concepto: 'SubTotal', importe: 0}],
}


var suggest_partners = {
    view: 'gridsuggest',
    id: 'grid_clients_found',
    name: 'grid_clients_found',
    body: {
        autoConfig: false,
        header: false,
        columns: [
            {id: 'id', hidden: true},
            {id: 'nombre', adjust: 'data'},
            {id: 'rfc', adjust: 'data'},
            {id: 'forma_pago', hidden: true},
            {id: 'uso_cfdi', hidden: true},
        ],
        dataFeed:function(text){
            if (text.length > 2){
                this.load('/values/client?name=' + text)
            }else{
                this.hide()
            }
        }
    }
}

var suggest_products = {
    view: 'gridsuggest',
    id: 'grid_products_found',
    name: 'grid_products_found',
    body: {
        autoConfig: false,
        header: true,
        columns: [
            {id: 'id', hidden: true},
            {id: 'clave', header: 'Clave', adjust: 'data'},
            {id: 'descripcion', header: 'Descripción', adjust: 'data'},
            {id: 'unidad', header: 'Unidad', adjust: 'data'},
            {id: 'valor_unitario', header: 'Valor Unitario', adjust: 'data',
                format: webix.i18n.priceFormat}
        ],
        dataFeed:function(text){
            if (text.length > 2){
                this.load('/values/product?name=' + text)
            }else{
                this.hide()
            }
        }
    }
}


var body_comprobante = {rows: [{
    cols: [
        {
            view: 'richselect',
            id: 'lst_tipo_comprobante',
            label: 'Tipo',
            labelPosition: 'top',
            required: true,
            value: 'I',
            options: [
                {id: 'I', value: 'Ingreso'},
                {id: 'E', value: 'Egreso'},
                {id: 'T', value: 'Traslado'}
            ]
        },
        {
            view: 'richselect',
            id: 'lst_serie',
            label: 'Serie',
            labelPosition: 'top',
            options: [],
        },
    ]},
    {view: 'richselect', id: 'lst_uso_cfdi', name: 'uso_cfdi', required: true,
        labelPosition: 'top', label: 'Uso del CFDI', options: []},
]}


var opt_metodo_pago = [
    {id: 'PUE', value: 'Pago en una sola exhibición'},
    {id: 'PPD', value: 'Pago en parcialidades o diferido'}
]


var body_opciones = {rows: [
    {view: 'richselect', id: 'lst_metodo_pago', label: 'Método de Pago',
        labelPosition: 'top', options: opt_metodo_pago, value: 'PUE',
        required: true},
    {view: 'richselect', id: 'lst_forma_pago', name: 'forma_pago',
        label: 'Forma de Pago', labelPosition: 'top', required: true,
        options: []},
    {view: 'text', id: 'txt_condicion_pago', label: 'Condiciones de Pago',
        labelPosition: 'top', suggest: []},
]}


var body_moneda = {cols: [
        {view: 'richselect', id: 'lst_moneda', label: 'Nombre',
            labelPosition: 'top', required: true, options: []},
        {view: 'text', type: 'text', id: 'txt_tipo_cambio', value: '1.00',
            label: 'Tipo de Cambio', labelPosition: 'top', required: true,
            invalidMessage: 'Captura un valor númerico', inputAlign: 'right',
            readonly: true}
]}


var body_regimen_fiscal = {
    view: 'richselect',
    id: 'lst_regimen_fiscal',
    required: true,
    options: []
}


var controls_generate = [
    {minHeight: 10, maxHeight: 10},
    toolbar_invoices_generate,
    {minHeight: 10, maxHeight: 10},
    {cols: [ {rows:[
        {view: 'fieldset', label: 'Buscar Cliente', body: {rows: [
            {cols: [
                {view:"search", id:"search_client_id", name:"search_client_id",
                    label:"por Clave", labelPosition:'top', maxWidth:200,
                    placeholder:'Captura la clave'},
                {view: 'search', id: 'search_client_name',
                    name: 'search_client_name', label: 'por Nombre o RFC',
                    labelPosition: 'top', suggest: suggest_partners,
                    placeholder: 'Captura al menos tres letras'},
            ]},
            {cols: [{
                view: 'label', id: 'lbl_client_title',
                    name: "lbl_client_title", label: 'Seleccionado: ',
                    autowidth:true},
                {view: 'label', id: 'lbl_client', name: 'lbl_client',
                    label: 'Ninguno'},
            ]}
        ]}},
        {view: 'fieldset', label: 'Buscar Producto', body: {rows: [
            {cols: [
                {view: "search", id: "search_product_id",
                    name: "search_product_id", label: "por Clave",
                    labelPosition:'top', maxWidth:200,
                    placeholder:'Presiona ENTER para buscar'},
                {view: "search", id: "search_product_name",
                    name: "search_product_name", label: "por Descripción o Clave",
                    labelPosition:'top', suggest: suggest_products,
                    placeholder:'Captura al menos tres letras'},
            ]},
        ]}}
    ]},
    {maxWidth: 10},
    {maxWidth: 300, rows: [
        {view: 'fieldset', label: 'Comprobante', body: body_comprobante},
        {view: 'fieldset', label: 'Opciones de Pago', body: body_opciones},
        {view: 'fieldset', id: 'fs_moneda', label: 'Moneda', body: body_moneda},
        {view: 'fieldset', id: 'fs_regimen_fiscal', label: 'Regimen Fiscal',
            body: body_regimen_fiscal},
        ]}
    ]},
    {view: 'label', label: 'Detalle', height: 30, align: 'left'},
    grid_details,
    {minHeight: 15, maxHeight: 15},
    {cols: [{}, grid_totals]},
    {minHeight: 15, maxHeight: 15},
    {margin: 20, cols: [{},
        {view: "button", id: "cmd_timbrar", label: "Timbrar", icon: 'ticket',
            type: 'iconButton', autowidth: true, align:"center"},
        {view: "button", id: 'cmd_prefactura', label: "PreFactura",
            type: 'iconButton', autowidth: true, align: 'center', icon: 'file-o'},
        {}]
    },
    {rows: [
        {template:"", type: "section" },
        {margin: 10, cols: [{},
            {view: 'button', id: 'cmd_close_invoice', label: 'Cancelar',
                type: 'danger', autowidth: true, align: 'center'}
            ]
        },
    ]}
]


var toolbar_preinvoices = [
    {view: 'button', id: 'cmd_facturar_preinvoice', label: 'Facturar',
        type: 'iconButton', autowidth: true, icon: 'pencil'},
    {},
    {view: "button", id: "cmd_delete_preinvoice", label: "Eliminar",
        type: "iconButton", autowidth: true, icon: "minus"},
]


var toolbar_prefilter = [
    {view: 'richselect', id: 'prefilter_year', label: 'Año', labelAlign: 'right',
        labelWidth: 50, width: 150, options: []},
    {view: 'richselect', id: 'prefilter_month', label: 'Mes', labelAlign: 'right',
        labelWidth: 50, width: 200, options: months},
]


var grid_preinvoices_cols = [
    {id: "id", header:"ID", hidden:true},
    {id: "folio", header: ["Folio", {content: "numberFilter"}], adjust: "data",
        sort:"int", css: "cell_right"},
    {id: "fecha", header: ["Fecha y Hora"],
        adjust: "data", sort: "date"},
    {id: "tipo_comprobante", header: ["Tipo", {content: "selectFilter"}],
        adjust: 'header', sort: 'string'},
    {id: 'total_mn', header: ['Total M.N.'], width: 150,
        sort: 'int', format: webix.i18n.priceFormat, css: 'right'},
    {id: "cliente", header: ["Razón Social", {content: "selectFilter"}],
        fillspace:true, sort:"string"},
    {id: 'pdf', header: 'PDF', adjust: 'data', template: get_icon('pdf')},
    {id: 'email', header: '', adjust: 'data', template: get_icon('email')}
]


var grid_preinvoices = {
    view: 'datatable',
    id: 'grid_preinvoices',
    select: 'row',
    adjust: true,
    footer: true,
    resizeColumn: true,
    autoheight: true,
    headermenu: true,
    columns: grid_preinvoices_cols
}


var controls_prefactura = [
    {view: 'toolbar', elements: toolbar_preinvoices},
    {view: 'toolbar', elements: toolbar_prefilter},
    grid_preinvoices,
]


var opt_tipo_proceso = [
    {id: 'Ordinario', value: 'Ordinario'},
    {id: 'Precampaña', value: 'Precampaña'},
    {id: 'Campaña', value: 'Campaña'},
]


var opt_tipo_comite = [
    {id: 'Ejecutivo Nacional', value: 'Ejecutivo Nacional'},
    {id: 'Ejecutivo Estatal', value: 'Ejecutivo Estatal'},
    {id: 'Directivo Estatal', value: 'Directivo Estatal'},
]


var controles_ine = [
    {maxHeight: 15},
    {cols: [{maxWidth: 15},
        {view: 'checkbox', id: 'chk_cfdi_usar_ine', labelWidth: 0,
            labelRight: 'Usar el complemento INE'},
        {}]},
    {maxHeight: 10},
    {cols: [{maxWidth: 15},
        {view: 'richselect', id: 'lst_ine_tipo_proceso', labelWidth: 150,
            label: 'Tipo de Proceso', options: opt_tipo_proceso,
            value: 'Ordinario'},
        {}]},
    {maxHeight: 10},
    {cols: [{maxWidth: 15},
        {view: 'richselect', id: 'lst_ine_tipo_comite', labelWidth: 150,
            label: 'Tipo de Comite', options: opt_tipo_comite,
            value: 'Ejecutivo Nacional'},
        {}]},
    {maxHeight: 10},
    {cols: [{maxWidth: 15},
        {view: 'text', id: 'txt_ine_idcontabilidad', name: 'ine_idcontabilidad',
            label: 'ID de Contabilidad: ', labelWidth: 150},
        {}]},
]


var controls_invoices = [
    {
        view: 'tabview',
        id: 'tv_invoice',
        //~ tabbar: {options: ['Generar', 'PreFacturas', 'INE']},
        animate: true,
        cells: [
            {id: 'Generar', rows: controls_generate},
            {id: 'PreFacturas', rows: controls_prefactura},
            {id: 'INE', rows: controles_ine},
        ]
    },
]


var form_invoice = {
    type: 'space',
    responsive: true,
    cols: [{
        view: 'form',
        id: 'form_invoice',
        complexData: true,
        scroll: true,
        elements: controls_invoices,
    }]
}


var multi_invoices = {
    id: 'multi_invoices',
    view: 'multiview',
    animate: true,
    cells:[
        {id: 'invoices_home', rows:[
            {view: 'toolbar', elements: toolbar_invoices},
            {view: 'toolbar', elements: toolbar_invoices_util},
            {view: 'toolbar', elements: toolbar_invoices_filter},
            grid_invoices,
        ]},
        {id: 'invoices_new', rows:[form_invoice]}
    ]
}


var app_invoices = {
    id: "app_invoices",
    rows:[
        {view: "template", id: "th_invoices", type: "header", template:"Administración de Facturas" },
        multi_invoices
    ],
}
