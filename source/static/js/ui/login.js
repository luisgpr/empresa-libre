
var msg_rfc = 'El RFC es requerido'
var msg_user = 'El usuario es requerido'
var msg_pass = 'La contraseña es requerida'

var form_controls = [
    {view: 'text', label: 'RFC', id: 'txt_rfc', name: 'rfc',
        labelPosition: 'top', required: true, invalidMessage: msg_rfc},
    {view: 'text', label: 'Usuario', id: 'txt_usuario', name: 'usuario',
        labelPosition: 'top', required: true, invalidMessage: msg_user},
    {view: 'text', label: 'Contraseña', id: 'txt_contra', name: 'contra',
        type: 'password', required: true, labelPosition: 'top',
        invalidMessage: msg_pass},
    {margin: 10, cols:[{}, {view: 'button', value: 'Iniciar Sesión',
        click: 'validate_login', hotkey: 'enter'}, {}]}
]


var msg_header = '<font color="#610B0B">Bienvenido a Empresa Libre</font>'

var ui_login = {
    rows: [
        {maxHeight: 50},
        {view: 'template', template: msg_header, maxHeight: 50, css: 'login_header'},
        {maxHeight: 50},
        {cols: [{}, {type: 'space', padding: 5,
            rows: [
                {view: 'template', type: 'header',
                    template: '<font color="#610B0B">Acceso al sistema</font>'},
                {
                    container: 'form_login',
                    view: 'form',
                    id: 'form_login',
                    width: 400,
                    elements: form_controls,
                    rules:{
                        rfc:function(value){ return value.trim() != '';},
                        usuario:function(value){ return value.trim() != '';},
                        contra:function(value){ return value.trim() != '';},
                    }
                },
            ]}, {}, ]
        },
    ]
}
