var gi = null


function configuracion_inicial(){
    webix.ajax().get('/values/admin', function(text, data){
        var values = data.json()
        show('cmd_ir_al_admin', values)
    })
    webix.ajax().get('/values/main', function(text, data){
        var values = data.json()
        $$('lbl_title_main').setValue(values.empresa)
    })
}


function cmd_ir_al_admin_click(){
    window.location = '/admin'
}


var controllers = {
    init: function(){
        //~ Main
        $$('menu_user').attachEvent('onMenuItemClick', menu_user_click);
        configuracion_inicial()

        //~ Partner
        $$('cmd_new_partner').attachEvent('onItemClick', cmd_new_partner_click);
        $$('cmd_new_contact').attachEvent('onItemClick', cmd_new_contact_click);
        $$('cmd_edit_partner').attachEvent('onItemClick', cmd_edit_partner_click);
        $$('cmd_delete_partner').attachEvent('onItemClick', cmd_delete_partner_click);
        $$('cmd_save_partner').attachEvent('onItemClick', cmd_save_partner_click);
        $$('cmd_cancel_partner').attachEvent('onItemClick', cmd_cancel_partner_click);
        $$('cmd_cancel_contact').attachEvent('onItemClick', cmd_cancel_contact_click);
        $$('codigo_postal').attachEvent('onKeyPress', postal_code_key_press);
        $$('codigo_postal').attachEvent('onTimedKeyPress', postal_code_key_up);
        $$('colonia').attachEvent('onFocus', colonia_on_focus)
        $$("tipo_persona").attachEvent( "onChange", opt_tipo_change)
        $$("es_cliente").attachEvent( "onChange", is_client_change)
        $$("es_proveedor").attachEvent( "onChange", is_supplier_change)
        $$("rfc").attachEvent( "onBlur", rfc_lost_focus)
        $$('multi').attachEvent('onViewChange', multi_change)
        //~ Products
        $$("cmd_new_product").attachEvent("onItemClick", cmd_new_product_click)
        $$("cmd_edit_product").attachEvent("onItemClick", cmd_edit_product_click)
        $$("cmd_delete_product").attachEvent("onItemClick", cmd_delete_product_click)
        $$("cmd_save_product").attachEvent("onItemClick", cmd_save_product_click)
        $$("cmd_cancel_product").attachEvent("onItemClick", cmd_cancel_product_click)
        $$("chk_automatica").attachEvent("onChange", chk_automatica_change)
        $$("valor_unitario").attachEvent("onChange", valor_unitario_change)
        $$("clave_sat").attachEvent('onSearchIconClick', clave_sat_icon_click)
        //~ Invoices
        $$('cmd_new_invoice').attachEvent("onItemClick", cmd_new_invoice_click)
        $$('cmd_refacturar').attachEvent("onItemClick", cmd_refacturar_click)
        $$('cmd_delete_invoice').attachEvent("onItemClick", cmd_delete_invoice_click)
        $$('cmd_timbrar').attachEvent('onItemClick', cmd_timbrar_click)
        $$('cmd_close_invoice').attachEvent('onItemClick', cmd_close_invoice_click)
        $$('search_client_id').attachEvent('onKeyPress', search_client_id_key_press)
        $$('grid_clients_found').attachEvent('onValueSuggest', grid_clients_found_click)
        $$('search_product_id').attachEvent('onKeyPress', search_product_id_key_press)
        $$('grid_products_found').attachEvent('onValueSuggest', grid_products_found_click)
        $$('grid_details').attachEvent('onItemClick', grid_details_click)
        $$('grid_details').attachEvent('onHeaderClick', grid_details_header_click)
        $$('grid_details').attachEvent('onBeforeEditStart', grid_details_before_edit_start)
        $$('grid_details').attachEvent('onBeforeEditStop', grid_details_before_edit_stop)
        $$('cmd_invoice_timbrar').attachEvent('onItemClick', cmd_invoice_timbrar_click)
        $$('cmd_invoice_sat').attachEvent('onItemClick', cmd_invoice_sat_click)
        $$('cmd_invoice_cancelar').attachEvent('onItemClick', cmd_invoice_cancelar_click)
        $$('grid_invoices').attachEvent('onItemClick', grid_invoices_click)
        $$('filter_year').attachEvent('onChange', filter_year_change)
        $$('filter_month').attachEvent('onChange', filter_month_change)
        $$('filter_dates').attachEvent('onChange', filter_dates_change)
        $$('cmd_prefactura').attachEvent('onItemClick', cmd_prefactura_click)
        $$('cmd_cfdi_relacionados').attachEvent('onItemClick', cmd_cfdi_relacionados_click)
        $$('lst_metodo_pago').attachEvent('onChange', lst_metodo_pago_change)
        $$('lst_moneda').attachEvent('onChange', lst_moneda_change)
        $$('lst_serie').attachEvent('onChange', lst_serie_change)

        var tb_invoice = $$('tv_invoice').getTabbar()
        tb_invoice.attachEvent('onChange', tb_invoice_change)
        $$('prefilter_year').attachEvent('onChange', prefilter_year_change)
        $$('prefilter_month').attachEvent('onChange', prefilter_month_change)
        $$('cmd_delete_preinvoice').attachEvent('onItemClick', cmd_delete_preinvoice_click)
        $$('cmd_facturar_preinvoice').attachEvent('onItemClick', cmd_facturar_preinvoice_click)
        $$('grid_preinvoices').attachEvent('onItemClick', grid_preinvoices_click)

        webix.extend($$('grid_invoices'), webix.ProgressBar)

        bancos_controllers.init()
    }
}


function get_uso_cfdi_to_table(){
    webix.ajax().sync().get('/values/usocfdi', function(text, data){
        var values = data.json()
        table_usocfdi.clear()
        table_usocfdi.insert(values)
    })
}


function get_partners(){
    webix.ajax().get('/partners', {}, {
        error: function(text, data, xhr) {
            msg_error('Error al consultar')
        },
        success: function(text, data, xhr) {
            var values = data.json();
            $$('grid_partners').clearAll();
            if (values.data){
                $$('grid_partners').parse(values.data, 'json');
            };
        }
    })
}


function get_products(){
    var grid = $$('grid_products')
    webix.ajax().get('/products', {}, {
        error: function(text, data, xhr) {
            msg_error('Error al consultar')
        },
        success: function(text, data, xhr) {
            var values = data.json();
            grid.clearAll();
            if (values.ok){
                grid.parse(values.rows, 'json');
            };
        }
    });
}


function menu_user_click(id, e, node){
    if (id == 1){
        window.location = '/logout';
        return
    }
}


function current_dates(){
    var fy = $$('filter_year')
    var fm = $$('filter_month')
    var pfy = $$('prefilter_year')
    var pfm = $$('prefilter_month')
    var d = new Date()

    fy.blockEvent()
    fm.blockEvent()
    pfy.blockEvent()
    pfm.blockEvent()

    fm.setValue(d.getMonth() + 1)
    pfm.setValue(d.getMonth() + 1)
    webix.ajax().sync().get('/values/filteryears', function(text, data){
        var values = data.json()
        fy.getList().parse(values[0])
        pfy.getList().parse(values[1])
        fy.setValue(d.getFullYear())
        pfy.setValue(d.getFullYear())
    })

    fy.unblockEvent()
    fm.unblockEvent()
    pfy.unblockEvent()
    pfm.unblockEvent()
}


function multi_change(prevID, nextID){
    if(nextID == 'app_partners'){
        active = $$('multi_partners').getActiveId()
        if(active == 'partners_home'){
            get_partners()
        }
        return
    }

    if(nextID == 'app_products'){
        active = $$('multi_products').getActiveId()
        if(active == 'products_home'){
            get_products()
        }
        return
    }

    if(nextID == 'app_bancos'){
        active = $$('multi_bancos').getActiveId()
        if(active == 'banco_home'){
            get_cuentas_banco()
        }
        return
    }

    if(nextID == 'app_invoices'){
        active = $$('multi_invoices').getActiveId()
        if(active == 'invoices_home'){
            current_dates()
            get_invoices()
            validar_timbrar()
        }
        gi = $$('grid_invoices')
        return
    }

}


function get_taxes(){
    webix.ajax().sync().get('/values/taxes', function(text, data){
        var values = data.json()
        table_taxes.clear()
        table_taxes.insert(values)
        $$("grid_product_taxes").clearAll()
        $$("grid_product_taxes").parse(values, 'json')
    })
}
