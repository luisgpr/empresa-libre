var msg = ''
var msg_importe = ''


var bancos_controllers = {
    init: function(){
        $$('lst_cuentas_banco').attachEvent('onChange', lst_cuentas_banco_change)
        $$('cmd_agregar_retiro').attachEvent('onItemClick', cmd_agregar_retiro_click)
        $$('cmd_agregar_deposito').attachEvent('onItemClick', cmd_agregar_deposito_click)
        $$('cmd_guardar_retiro').attachEvent('onItemClick', cmd_guardar_retiro_click)
        $$('cmd_guardar_deposito').attachEvent('onItemClick', cmd_guardar_deposito_click)
        $$('cmd_cancelar_movimiento').attachEvent('onItemClick', cmd_cancelar_movimiento_click)
        $$('txt_retiro_importe').attachEvent('onChange', txt_retiro_importe_change)
        $$('txt_deposito_importe').attachEvent('onChange', txt_deposito_importe_change)
        $$('grid_cfdi_este_deposito').attachEvent('onAfterDrop', grid_cfdi_este_deposito_after_drop)
        $$('grid_cfdi_por_pagar').attachEvent('onAfterDrop', grid_cfdi_por_pagar_after_drop)
        $$('grid_cfdi_este_deposito').attachEvent('onBeforeEditStop', grid_cfdi_este_deposito_before_edit_stop)
        $$('grid_cfdi_este_deposito').attachEvent('onAfterEditStop', grid_cfdi_este_deposito_after_edit_stop)
        set_year_month()
    }
}


function set_year_month(){
    var d = new Date()
    var y =  $$('filtro_cuenta_year')
    var m =  $$('filtro_cuenta_mes')

    webix.ajax().get('/values/cuentayears', {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            y.getList().parse(values)
            y.blockEvent()
            m.blockEvent()
            y.setValue(d.getFullYear())
            m.setValue(d.getMonth() + 1)
            y.unblockEvent()
            m.unblockEvent()
        }
    })
}


function get_cuentas_banco(){
    var list =  $$('lst_cuentas_banco')

    webix.ajax().get('/cuentasbanco', {'tipo': 1}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                list.getList().parse(values.rows)
                list.blockEvent()
                list.setValue(values.rows[0].id)
                list.unblockEvent()
                $$('txt_cuenta_moneda').setValue(values.moneda)
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
            }
        }
    })
}


function get_estado_cuenta(rango){
    if(rango == undefined){
        var filtro = {
            cuenta: $$('lst_cuentas_banco').getValue(),
            year: $$('filtro_cuenta_year').getValue(),
            mes: $$('filtro_cuenta_mes').getValue(),
        }
    }else{
        var filtro = {
            cuenta: $$('lst_cuentas_banco').getValue(),
            fechas: rango,
        }
    }

    var grid = $$('grid_cuentabanco')

    webix.ajax().get('/movbanco', filtro, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            grid.clearAll()
            if (values.ok){
                grid.parse(values.rows, 'json')
            }
        }
    })
}


function get_saldo_cuenta(){
    var id = $$('lst_cuentas_banco').getValue()
    webix.ajax().get('/values/saldocuenta', {id: id}, function(text, data){
        var value = data.json()
        if(value){
            $$('txt_cuenta_saldo').setValue(value)
        }else{
            msg = 'No se pudo consultar el saldo'
            msg_error(msg)
        }
    })
}


function lst_cuentas_banco_change(nv, ov){
    show('Cuenta change')
}


function get_bancos_forma_pago(retiro){
    webix.ajax().get('/values/formapago', {}, function(text, data){
        var values = data.json()
        if(retiro){
            $$('lst_retiro_forma_pago').getList().parse(values)
        }else{
            $$('lst_deposito_forma_pago').getList().parse(values)
        }
    })
}


function get_facturas_por_pagar(){
    var grid1 = $$('grid_cfdi_este_deposito')
    var grid2 = $$('grid_cfdi_por_pagar')

    var ids = []
    grid1.data.each(function(obj){
        ids.push(obj.id)
    })

    webix.ajax().get('/invoices', {'opt': 'porpagar', 'ids': ids}, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            grid2.clearAll()
            if (values.ok){
                grid2.parse(values.rows, 'json')
            }
        }
    })
}


function cmd_agregar_retiro_click(){
    get_bancos_forma_pago(true)
    $$('multi_bancos').setValue('banco_retiro')
}


function cmd_agregar_deposito_click(){
    msg_importe = ''
    get_bancos_forma_pago(false)
    get_facturas_por_pagar()
    $$('multi_bancos').setValue('banco_deposito')
}


function validate_retiro(values){
    var importe = values.retiro_importe.replace('$', '').replace(',', '').trim()

    if(!importe){
        msg = 'El importe es requerido'
        msg_error(msg)
        return false
    }
    importe = parseFloat(importe).round(2)
    if(importe <= 0){
        msg = 'El importe debe ser mayor a cero'
        msg_error(msg)
        return false
    }

    if(!values.retiro_descripcion.trim()){
        msg = 'La descripción es requerida'
        msg_error(msg)
        return false
    }

    var today = new Date()
    if(values.retiro_fecha > today){
        msg = 'Fecha inválida, es una fecha futura'
        msg_error(msg)
        return
    }

    var horas = $$('time_retiro').getText().split(':')
    var seg = parseInt(horas[2])
    var min = parseInt(horas[1])
    var horas = parseInt(horas[0])

    if(horas > 23){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(min > 59){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(seg > 59){
        focus('time_retiro')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }

    return true
}


function guardar_retiro(values){
    var form = $$('form_banco_retiro')

    var importe = get_float(values.retiro_importe)
    var data = new Object()
    data['cuenta'] = $$('lst_cuentas_banco').getValue()
    data['fecha'] = values.retiro_fecha
    data['hora'] = $$('time_retiro').getText()
    data['numero_operacion'] = values.retiro_referencia.trim()
    data['forma_pago'] = $$('lst_retiro_forma_pago').getValue()
    data['retiro'] = importe
    data['deposito'] = 0.0
    data['descripcion'] = values.retiro_descripcion

    webix.ajax().post('/movbanco', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
                $$('multi_bancos').setValue('banco_home')
                form.setValues({})
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_guardar_retiro_click(){
    var form = $$('form_banco_retiro')

    if(!form.validate()) {
        msg_error('Valores inválidos')
        return
    }

    var values = form.getValues()
    if(!validate_retiro(values)){
        return
    }

    msg = 'Todos los datos son correctos.<br><br>¿Deseas agregar este retiro?'
    webix.confirm({
        title: 'Guardar Retiro',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if(result){
                guardar_retiro(values)
            }
        }
    })
}


function txt_retiro_importe_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}

function txt_deposito_importe_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function actualizar_deposito(grid){
    grid.sort("#fecha#", "desc", "date")

    var suma = 0
    var descripcion = ''
    grid.data.each(function(obj){
        descripcion += 'Pago de la factura: ' + obj.serie + obj.folio + ' del '
        descripcion += 'cliente: ' + obj.cliente + '\n'
        if(obj.importe == undefined){
            obj.importe = obj.saldo
        }
        suma += obj.importe.to_float()
    })
    $$('txt_deposito_importe').setValue(suma)
    $$('deposito_descripcion').setValue(descripcion.slice(0, -1))
    grid.refresh()
}


function grid_cfdi_por_pagar_after_drop(context, native_event){
    var grid = $$('grid_cfdi_este_deposito')
    actualizar_deposito(grid)
}


function grid_cfdi_este_deposito_after_drop(context, native_event){
    var grid = $$('grid_cfdi_este_deposito')
    actualizar_deposito(grid)
}


function grid_cfdi_este_deposito_after_edit_stop(state, editor, ignoreUpdate){
    var grid = $$('grid_cfdi_este_deposito')

    var suma = 0
    grid.data.each(function(obj){
        suma += obj.importe.to_float()
    })
    $$('txt_deposito_importe').setValue(suma)
}


function grid_cfdi_este_deposito_before_edit_stop(state, editor){
    var grid = $$('grid_cfdi_este_deposito')
    var row = grid.getItem(editor.row)

    if(editor.column == 'importe'){
        var importe = parseFloat(state.value)
        if(isNaN(importe)){
            msg = 'El importe a pagar debe ser un número'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        if(importe <= 0){
            msg = 'El importe a pagar debe ser mayor a cero'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
        var saldo = row['saldo'].to_float()
        if(importe > saldo){
            msg = 'El importe a pagar no puede ser mayor al saldo de la factura'
            msg_error(msg)
            grid.blockEvent()
            state.value = state.old
            grid.editCancel()
            grid.unblockEvent()
            return true
        }
    }
}


function validate_deposito(values){
    var grid = $$('grid_cfdi_este_deposito')
    var importe = values.deposito_importe.to_float()

    if(!importe){
        msg = 'El importe es requerido'
        msg_error(msg)
        return false
    }

    if(importe <= 0){
        msg = 'El importe debe ser mayor a cero'
        msg_error(msg)
        return false
    }

    if(!values.deposito_descripcion.trim()){
        msg = 'La descripción es requerida'
        msg_error(msg)
        return false
    }

    var today = new Date()
    if(values.deposito_fecha > today){
        msg = 'Fecha inválida, es una fecha futura'
        msg_error(msg)
        return
    }

    var horas = $$('time_deposito').getText().split(':')
    var seg = parseInt(horas[2])
    var min = parseInt(horas[1])
    var horas = parseInt(horas[0])

    if(horas > 23){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(min > 59){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }
    if(seg > 59){
        focus('time_deposito')
        msg = 'Hora inválida'
        msg_error(msg)
        return false
    }

    if(grid.count()){
        var suma = 0
        grid.data.each(function(obj){
            var tmp = obj.importe.to_float()
            if(tmp <= 0){
                msg = 'El importe de la factura: ' + obj.serie + obj.folio + ' no puede ser menor a cero'
                msg_error(msg)
                return false
            }
            suma += tmp
        })
        if(suma > importe){
            msg = 'La suma del pago de facturas, no puede ser mayor al deposito'
            msg_error(msg)
            return false
        }
        if(suma < importe){
            msg_importe = 'El importe del depósito en mayor a la suma de facturas. '
            msg_importe += 'Asegurate de que esto sea correcto'
        }
    }

    return true
}


function guardar_deposito(values){
    var form = $$('form_banco_deposito')
    var grid = $$('grid_cfdi_este_deposito')

    var data = new Object()
    data['cuenta'] = $$('lst_cuentas_banco').getValue()
    data['fecha'] = values.deposito_fecha
    data['hora'] = $$('time_deposito').getText()
    data['numero_operacion'] = values.deposito_referencia.trim()
    data['forma_pago'] = $$('lst_deposito_forma_pago').getValue()
    data['deposito'] = values.deposito_importe.to_float()
    data['retiro'] = 0.0
    data['descripcion'] = values.deposito_descripcion

    if(grid.count()){
        var ids = new Object()
        grid.data.each(function(obj){
            ids[obj.id] = obj.importe.to_float()
        })
        data['ids'] = ids
    }

    webix.ajax().post('/movbanco', data, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json()
            if(values.ok){
                $$('txt_cuenta_saldo').setValue(values.saldo)
                get_estado_cuenta()
                $$('multi_bancos').setValue('banco_home')
                form.setValues({})
                grid.clearAll()
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_guardar_deposito_click(){
    var form = $$('form_banco_deposito')
    var grid = $$('grid_cfdi_este_deposito')

    if(!form.validate()) {
        msg_error('Valores inválidos')
        return
    }

    var values = form.getValues()
    if(!validate_deposito(values)){
        return
    }

    if(!grid.count()){
        msg = 'Todos los datos son correctos<br>br>'
        msg = 'El depósito no tiene facturas relacionadas<br><br>¿Estás '
        msg += ' seguro de guardar del depósito sin facturas relacionadas?'
        webix.confirm({
            title: 'Guardar depósito',
            ok: 'Si',
            cancel: 'No',
            type: 'confirm-error',
            text: msg,
            callback:function(result){
                if(result){
                    guardar_deposito(values)
                }
            }
        })
    }else{
        if(!msg_importe){
            msg_importe = 'Se van a relacionar ' + grid.count() + ' facturas.'
        }
        msg = 'Todos los datos son correctos.<br><br>' + msg_importe + '<br><br>'
        msg += '¿Deseas agregar este depósito?'
        webix.confirm({
            title: 'Guardar depósito',
            ok: 'Si',
            cancel: 'No',
            type: 'confirm-error',
            text: msg,
            callback:function(result){
                if(result){
                    guardar_deposito(values)
                }
            }
        })
    }
}


function cancelar_movimiento(id){
    var grid = $$('grid_cuentabanco')

    webix.ajax().del('/movbanco', {id: id}, function(text, xml, xhr){
        if(xhr.status == 200){
            get_estado_cuenta()
            get_saldo_cuenta()
            msg_ok('Movimiento cancelado correctamente')
        }else{
            msg_error('No se pudo eliminar')
        }
    })
}


function cmd_cancelar_movimiento_click(){
    var grid = $$('grid_cuentabanco')

    var row = grid.getSelectedItem()
    if(row == undefined){
        msg_error('Selecciona un movimiento')
        return
    }
    if(row.descripcion == 'Saldo inicial'){
        msg_error('No es posible eliminar el saldo inicial')
        return
    }

    var msg = '¿Estás seguro de cancelar el movimiento seleccionado?'
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>'
    webix.confirm({
        title: 'Cancelar Movimiento',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback: function(result){
            if (result){
                cancelar_movimiento(row['id'])
            }
        }
    })
}