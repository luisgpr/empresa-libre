

function configurar_productos(){
    webix.ajax().get('/config', {'fields': 'productos'}, {
        error: function(text, data, xhr) {
            msg = 'Error al consultar'
            msg_error(msg)
        },
        success: function(text, data, xhr) {
            var values = data.json()
            //~ showvar(values)
            show('cuenta_predial', values.chk_config_cuenta_predial)
        }
    })
}


function get_categorias(){
    webix.ajax().sync().get('/values/categorias', function(text, data){
        var values = data.json()
        $$('categoria').getList().parse(values, 'plainjs')
    })
}


function cmd_new_product_click(id, e, node){
    configurar_productos()
    $$('form_product').setValues({
        id: 0, es_activo_producto: true})
    add_config({'key': 'id_product', 'value': ''})
    get_new_key()
    get_taxes()
    get_categorias()
    $$('grid_products').clearSelection()
    $$('unidad').getList().load('/values/unidades')
    $$("multi_products").setValue("product_new")
}


function cmd_edit_product_click(id, e, node){
    configurar_productos()
    var grid = $$('grid_products')
    var row = grid.getSelectedItem()
    if(row == undefined){
        msg_error('Selecciona un Producto')
        return
    }

    get_taxes()
    $$('categoria').getList().load('/values/categorias')
    $$('unidad').getList().load('/values/unidades')

    webix.ajax().get('/products', {id:row['id']}, {
        error: function(text, data, xhr) {
            msg_error()
        },
        success: function(text, data, xhr){
            var values = data.json()
            $$('form_product').setValues(values.row)
            add_config({'key': 'id_product', 'value': values.row.id})
            for(i = 0; i < values.taxes.length; i++){
                $$('grid_product_taxes').select(values.taxes[i], true)
            }
        }
    })
    $$('multi_products').setValue('product_new')

};


function delete_product(id){
    webix.ajax().del('/products', {id:id}, function(text, xml, xhr){
        var msg = 'Producto eliminado correctamente'
        if(xhr.status == 200){
            $$('grid_products').remove(id)
            msg_ok(msg)
        }else{
            msg = 'No se pudo eliminar'
            msg_error(msg)
        }
    })
}


function cmd_delete_product_click(id, e, node){
    var row = $$('grid_products').getSelectedItem()
    if (row == undefined){
        msg_error('Selecciona un Producto')
        return
    }

    var msg = '¿Estás seguro de eliminar el Producto?<BR><BR>'
    msg += '(' + row['clave'] + ') ' + row['descripcion']
    msg += '<BR><BR>ESTA ACCIÓN NO SE PUEDE DESHACER<BR><BR>Se recomienda '
    msg += 'solo desactivar el producto en vez de eliminar'
    webix.confirm({
        title: 'Eliminar Producto',
        ok: 'Si',
        cancel: 'No',
        type: 'confirm-error',
        text: msg,
        callback:function(result){
            if (result){
                delete_product(row['id'])
            }
        }
    })
}


function validate_sat_key_product(key, text){
    var result = false
    webix.ajax().sync().get('/values/satkey', {key:key}, function(text, data){
        result = data.json()
    })
    if(text){
        if(result.ok){
            return '<b>' + result.text + '</b>'
        }else{
            return '<b><font color="red">' + result.text + '</font></b>'
        }
    }
    return result.ok
}


function update_grid_products(values){
    var msg = 'Producto agregado correctamente'
    if(values.new){
        $$('form_product').clear()
        $$('grid_products').add(values.row)
    }else{
        msg = 'Producto actualizado correctamente'
        $$("grid_products").updateItem(values.row['id'], values.row)
    }
    $$('multi_products').setValue('products_home')
    msg_ok(msg)
}


function cmd_save_product_click(id, e, node){
    var msg = ''
    var form = this.getFormView()

    if(!form.validate()){
        msg_error('Valores inválidos')
        return
    }

    var rows = $$('grid_product_taxes').getSelectedId(true, true)
    if (rows.length == 0){
        msg_error('Selecciona un impuesto')
        return
    }

    var values = form.getValues();

    if (!validate_sat_key_product(values.clave_sat, false)){
        msg_error('La clave SAT no existe')
        return
    }

    values['taxes'] = JSON.stringify(rows)
    webix.ajax().sync().post('products', values, {
        error:function(text, data, XmlHttpRequest){
            msg = 'Ocurrio un error, consulta a soporte técnico'
            msg_error(msg)
        },
        success:function(text, data, XmlHttpRequest){
            var values = data.json();
            if (values.ok) {
                update_grid_products(values)
            }else{
                msg_error(values.msg)
            }
        }
    })
}


function cmd_cancel_product_click(id, e, node){

    $$("multi_products").setValue("products_home")

};


function chk_automatica_change(new_value, old_value){
    var value = Boolean(new_value)
    if (value){
        var value = get_config('id_product')
        if(value){
            $$("clave").setValue(value)
            $$("clave").refresh()
        }else{
            get_new_key()
        }
        $$("clave").config.readonly = true
        $$('form_product').focus('clave_sat')
    } else {
        $$("clave").setValue('')
        $$("clave").config.readonly = false
        $$('form_product').focus('clave')
    }
    $$("clave").refresh()
}


function get_new_key(){
    webix.ajax().get('/values/newkey', {
        error: function(text, data, xhr) {
            msg_error(text)
        },
        success: function(text, data, xhr) {
            var values = data.json();
            $$("clave").setValue(values.value)
            $$("clave").refresh()
        }
    })
}


function valor_unitario_change(new_value, old_value){
    if(!isFinite(new_value)){
        this.config.value = old_value
        this.refresh()
    }
}


function clave_sat_icon_click(){
    show('Buscar SAT')
}
